package ro.tuc.tp.polynomialcalculator.controller;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

import ro.tuc.tp.polynomialcalculator.model.Polynomial;
import ro.tuc.tp.polynomialcalculator.util.Parser;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.GetMapping;


@RestController
public class Controller {
    @GetMapping("api/calculator")
    public String getResult(@RequestParam(value = "polynomial1",required = true) String polynomial1, 
                                @RequestParam(value = "polynomial2", required = false) String polynomial2, 
                                @RequestParam(value = "operation", required = true) String operation) {
        ArrayList<Polynomial> result = new ArrayList<>();
        ObjectMapper objectMapper = new ObjectMapper();
        Parser parser = new Parser(polynomial1, operation, polynomial2);
        try {
            parser.executeOperation(result);
            //return result.get(0).toString();
            return objectMapper.writeValueAsString(result.toString());
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }

}

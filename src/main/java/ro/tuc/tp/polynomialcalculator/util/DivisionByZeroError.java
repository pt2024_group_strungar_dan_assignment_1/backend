package ro.tuc.tp.polynomialcalculator.util;

public class DivisionByZeroError extends Exception{
    public DivisionByZeroError(String message){
        super(message);
    }
}

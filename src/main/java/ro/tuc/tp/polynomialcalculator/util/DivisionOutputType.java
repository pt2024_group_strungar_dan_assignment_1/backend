package ro.tuc.tp.polynomialcalculator.util;

public enum DivisionOutputType {
    QUOTIENT, REMAINDER
}

package ro.tuc.tp.polynomialcalculator.util;

import java.util.ArrayList;

import ro.tuc.tp.polynomialcalculator.model.AdditionOP;
import ro.tuc.tp.polynomialcalculator.model.DerivativeOP;
import ro.tuc.tp.polynomialcalculator.model.DivisionOP;
import ro.tuc.tp.polynomialcalculator.model.IntegrationOP;
import ro.tuc.tp.polynomialcalculator.model.MultiplicationOP;
import ro.tuc.tp.polynomialcalculator.model.Polynomial;
import ro.tuc.tp.polynomialcalculator.model.SubtractionOP;

public class Parser {

    private String rawPolynomial1;
    private String operation;
    private String rawPolynomial2;

    public Parser(String rawPolynomial1, String operation, String rawPolynomial2){
        this.rawPolynomial1 = rawPolynomial1;
        this.operation = operation;
        this.rawPolynomial2 = rawPolynomial2;
        
    }

    public boolean validateExpression(String expression){
        //if(expression.matches("([-+]?\\d*x\\^\\d*)|([-+]?\\d*x)|([-+]?\\d+)")) return true;
        return true;
    }

    public void executeOperation(ArrayList<Polynomial> result) throws UnknownCommandError, InvalidExpressionError, DivisionByZeroError{
        if(validateExpression(rawPolynomial1)){
            if(rawPolynomial2 != null && validateExpression(rawPolynomial2)){
                Polynomial polynomial1 = new Polynomial(rawPolynomial1);
                Polynomial polynomial2 = new Polynomial(rawPolynomial2);
                switch(operation){
                    case "addition" : result.add(AdditionOP.execute(polynomial1, polynomial2)); return;
                    case "subtraction" : result.add(SubtractionOP.execute(polynomial1, polynomial2)); return;
                    case "multiplication" : result.add(MultiplicationOP.execute(polynomial1, polynomial2)); return;
                    case "division" : result.add(DivisionOP.execute(polynomial1, polynomial2, DivisionOutputType.QUOTIENT));
                                 result.add(DivisionOP.execute(polynomial1, polynomial2, DivisionOutputType.REMAINDER));
                                 return;
                    case "derivation" : result.add(DerivativeOP.execute(polynomial1)); return;
                    case "integration" : result.add(IntegrationOP.execute(polynomial1)); return;
                    default : throw new UnknownCommandError("Unknown operation");
                }
            }
            else throw new InvalidExpressionError("Format error in polynomial expression");
        }
        else throw new InvalidExpressionError("Format error in polynomial expression");
    }
}

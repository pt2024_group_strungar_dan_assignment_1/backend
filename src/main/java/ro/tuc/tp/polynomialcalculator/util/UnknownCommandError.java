package ro.tuc.tp.polynomialcalculator.util;

public class UnknownCommandError extends Exception{
    public UnknownCommandError(String message) {
        super(message);
    }
}

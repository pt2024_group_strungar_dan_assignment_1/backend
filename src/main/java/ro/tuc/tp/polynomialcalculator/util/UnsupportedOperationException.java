package ro.tuc.tp.polynomialcalculator.util;

public class UnsupportedOperationException extends Exception{
    
        public UnsupportedOperationException(String message) {
            super(message);
        }
}

package ro.tuc.tp.polynomialcalculator.util;

public class InvalidExpressionError extends Exception{
    public InvalidExpressionError(String message) {
        super(message);
    }
}

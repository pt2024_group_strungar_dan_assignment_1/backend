package ro.tuc.tp.polynomialcalculator.model;

import java.util.Map;
import java.util.SortedMap;

public class DerivativeOP {

    private DerivativeOP() {
    }

    public static Polynomial execute(Polynomial polynomial) {
        SortedMap<Integer, Number> terms = polynomial.getTerms();
        Polynomial derivedPolynomial = new Polynomial();
        for (Map.Entry<Integer, Number> term : terms.entrySet()) {
            Integer degree = term.getKey();
            if (degree != 0) {
                Number newCoefficient = degree * terms.get(degree).intValue();
                derivedPolynomial.getTerms().put(degree - 1, newCoefficient);
            }
        }
        return derivedPolynomial;
    }
}

package ro.tuc.tp.polynomialcalculator.model;

import java.util.SortedMap;
import java.util.TreeMap;
import java.util.regex.Pattern;
import java.util.Iterator; 

import java.util.regex.Matcher;

public class Polynomial {

    private TreeMap<Integer, Number> terms;

    public Polynomial(String expression) {
        terms = new TreeMap<>();
        String monomialPattern = "\\b(\\d*x\\^?\\d*)|(\\d+)|[-+]";
        Pattern pattern = Pattern.compile(monomialPattern);
        Matcher matcher = pattern.matcher(expression);
        Integer sign = 1;
        while(matcher.find()){
            if(matcher.group().matches("[-+]")){
                if(matcher.group().equals("-"))
                    sign = -1;
                else 
                    sign = 1;
            } else {
                if(matcher.group().contains("x^")){
                    String[] monomial = matcher.group().split("x\\^");
                    Integer degree = Integer.parseInt(monomial[1]);
                    if(monomial[0].equals("")){
                        monomial[0] = "1";
                    }
                    Number coefficient = sign * Integer.parseInt(monomial[0]);
                    if(terms.containsKey(degree))
                        coefficient = coefficient.intValue() + terms.get(degree).intValue();
                    terms.put(degree, coefficient);
                } else if(matcher.group().contains("x")){
                    String[] monomial = matcher.group().split("x");
                    Integer degree = 1;
                    Number coefficient;
                    if(monomial.length == 0){
                        coefficient = sign;
                    }
                    else 
                        coefficient = sign * Integer.parseInt(monomial[0]);
                    if(terms.containsKey(degree))
                        coefficient = coefficient.intValue() + terms.get(degree).intValue();
                    terms.put(degree, coefficient);
                } else {
                    Integer degree = 0;
                    Number coefficient = sign * Integer.parseInt(matcher.group());
                    if(terms.containsKey(degree))
                        coefficient = coefficient.intValue() + terms.get(degree).intValue();
                    terms.put(degree, coefficient);
                }
            }
        }
    }

    public Polynomial() {
        terms = new TreeMap<>();
    }

    public Number getByDegree(Integer degree){
        return terms.get(degree);
    }

    public SortedMap<Integer, Number> getTerms() {
        return terms;
    }

    public void addTerm(Integer degree, Number coefficient){
        terms.put(degree, coefficient);
    }

    public void deepCopy(Polynomial polynomial){
        this.getTerms().clear();
        for(Integer degree : polynomial.getTerms().keySet()){
            terms.put(degree, polynomial.getTerms().get(degree));
        }
    }
    
    public Integer getDegree(){
        if(terms.isEmpty())
            return 0;
        else
            return terms.lastKey();
    }

    public void cleanUp(){
        Iterator<Integer> iterator = terms.keySet().iterator();
        while (iterator.hasNext()) {
            Integer degree = iterator.next();
            if (terms.get(degree).doubleValue() == 0) {
                iterator.remove(); // Safe removal using iterator's remove method
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (Integer degree : terms.keySet()) {
            Number coefficient = terms.get(degree);
            if (coefficient.doubleValue() != 0) { // Exclude terms with coefficient 0
                if (result.length() > 0) {
                    result.append(" + "); // Add "+" between terms
                }
                if (degree == 0) {
                    result.append(coefficient); // Constant term
                } else {
                    if (coefficient.intValue() != 1) {
                        result.append(coefficient); // Coefficient
                    }
                    result.append("x");
                    if (degree != 1) {
                        result.append("^").append(degree); // Degree
                    }
                }
            }
        }
        if (result.length() == 0) { // Handle case when polynomial is empty or all terms are 0
            result.append("0");
        }
        return result.toString();
    }
    
}

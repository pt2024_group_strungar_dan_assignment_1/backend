package ro.tuc.tp.polynomialcalculator.model;

import java.util.SortedMap;
import java.util.Map;

public class IntegrationOP {
    private IntegrationOP() {
    }
    
    public static Polynomial execute(Polynomial polynomial) {
        SortedMap<Integer, Number> terms = polynomial.getTerms();
        Polynomial integratedPolynomial = new Polynomial();
        for(Map.Entry<Integer, Number> term : terms.entrySet()){
            Integer degree = term.getKey();
            Number newCoefficient = terms.get(degree).doubleValue() / (degree + 1);
            integratedPolynomial.getTerms().put(degree + 1, newCoefficient);
        }
        return integratedPolynomial;
    }

}

package ro.tuc.tp.polynomialcalculator.model;

import ro.tuc.tp.polynomialcalculator.util.DivisionByZeroError;
import ro.tuc.tp.polynomialcalculator.util.DivisionOutputType;

public class DivisionOP {
    private DivisionOP() {
    }
    
    public static Polynomial execute(Polynomial polynomial1, Polynomial polynomial2, DivisionOutputType type) throws DivisionByZeroError {
        if(polynomial2.getDegree() != 0){
            Polynomial quotient = new Polynomial();
            Polynomial remainder = new Polynomial();
            remainder.deepCopy(polynomial1);
            while(remainder.getDegree() != 0 && remainder.getDegree() >= polynomial2.getDegree()){
                Polynomial temp = new Polynomial();
                temp.addTerm(remainder.getDegree() - polynomial2.getDegree(), remainder.getTerms().get(remainder.getDegree()).doubleValue() / polynomial2.getTerms().get(polynomial2.getDegree()).doubleValue());
                quotient = AdditionOP.execute(quotient, temp);
                remainder = SubtractionOP.execute(remainder, temp = MultiplicationOP.execute(polynomial2, temp));
                remainder.cleanUp();
            }
            if(type == DivisionOutputType.QUOTIENT) return quotient;
            else return remainder;
        }
        else throw new DivisionByZeroError("Division by zero is not allowed");
    }
}

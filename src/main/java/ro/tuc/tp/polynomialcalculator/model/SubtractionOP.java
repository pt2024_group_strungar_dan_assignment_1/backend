package ro.tuc.tp.polynomialcalculator.model;

import java.util.Map;
import java.util.SortedMap;

public class SubtractionOP{

    private SubtractionOP() {
    }

    public static Polynomial execute(Polynomial polynomial1, Polynomial polynomial2) {
        SortedMap<Integer, Number> terms1 = polynomial1.getTerms();
        SortedMap<Integer, Number> terms2 = polynomial2.getTerms();
        for (Map.Entry<Integer, Number> term : terms2.entrySet()) {
            if(terms1.containsKey(term.getKey()))
                polynomial1.getTerms().put(term.getKey(), term.getValue().doubleValue() - terms1.get(term.getKey()).doubleValue());
            else
                polynomial1.addTerm(term.getKey(), (-1)*term.getValue().doubleValue());
        }
        return polynomial1;

    }

}

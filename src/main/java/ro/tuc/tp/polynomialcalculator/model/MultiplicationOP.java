package ro.tuc.tp.polynomialcalculator.model;

import java.util.Map;
import java.util.SortedMap;

public class MultiplicationOP {
    private MultiplicationOP() {
    }

    public static Polynomial execute(Polynomial polynomial1, Polynomial polynomial2) {
        Polynomial result = new Polynomial();
        SortedMap<Integer, Number> terms1 = polynomial1.getTerms();
        SortedMap<Integer, Number> terms2 = polynomial2.getTerms();
        for(Map.Entry<Integer,Number> entry1 : terms1.entrySet()){
            for(Map.Entry<Integer,Number> entry2 : terms2.entrySet()){
                Integer degree = entry1.getKey() + entry2.getKey();
                Number coefficient = entry1.getValue().doubleValue() * entry2.getValue().doubleValue();
                if(result.getTerms().containsKey(degree))
                    result.getTerms().put(degree, coefficient.doubleValue() + result.getTerms().get(degree).doubleValue());
                else
                    result.addTerm(degree, coefficient);
            }
        }
        return result;
    }
}

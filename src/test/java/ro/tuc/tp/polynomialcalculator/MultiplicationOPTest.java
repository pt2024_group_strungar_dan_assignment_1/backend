package ro.tuc.tp.polynomialcalculator;

import org.junit.jupiter.api.Test;
import ro.tuc.tp.polynomialcalculator.model.MultiplicationOP;
import ro.tuc.tp.polynomialcalculator.model.Polynomial;

import java.util.TreeMap;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MultiplicationOPTest {

    @Test
    public void testExecute_MultiplyPolynomials() {
        // Arrange
        Polynomial polynomial1 = new Polynomial();
        polynomial1.addTerm(2, 3); // 3x^2
        polynomial1.addTerm(1, 2); // 2x
        polynomial1.addTerm(0, -5); // -5

        Polynomial polynomial2 = new Polynomial();
        polynomial2.addTerm(3, 1); // x^3
        polynomial2.addTerm(2, -4); // -4x^2
        polynomial2.addTerm(1, 6); // 6x

        // Act
        Polynomial result = MultiplicationOP.execute(polynomial1, polynomial2);

        // Assert
        TreeMap<Integer, Number> expectedResult = new TreeMap<>();
        expectedResult.put(5, 3.0); // 3x^5
        expectedResult.put(4, -10.0); // -12x^4
        expectedResult.put(3, 5.0); // 19x^3
        expectedResult.put(2, 32.0); // -22x^2
        expectedResult.put(1, -30.0); // 12x

        assertEquals(expectedResult, result.getTerms());
    }

    @Test
    public void testExecute_MultiplyByZero_ReturnsZero() {
        // Arrange
        Polynomial polynomial1 = new Polynomial();
        polynomial1.addTerm(2, 3); // 3x^2
        polynomial1.addTerm(1, 2); // 2x
        polynomial1.addTerm(0, -5); // -5

        Polynomial polynomial2 = new Polynomial(); // Zero polynomial

        // Act
        Polynomial result = MultiplicationOP.execute(polynomial1, polynomial2);

        // Assert
        TreeMap<Integer, Number> expectedResult = new TreeMap<>();
        //expectedResult.put(0, 0); // Result should be zero polynomial

        assertEquals(expectedResult, result.getTerms());
    }

    @Test
    public void testExecute_MultiplyByConstant_ReturnsScaledPolynomial() {
        // Arrange
        Polynomial polynomial1 = new Polynomial();
        polynomial1.addTerm(2, 3); // 3x^2
        polynomial1.addTerm(1, 2); // 2x
        polynomial1.addTerm(0, -5); // -5

        Polynomial polynomial2 = new Polynomial();
        polynomial2.addTerm(0, 4); // 4 (constant)

        // Act
        Polynomial result = MultiplicationOP.execute(polynomial1, polynomial2);

        // Assert
        TreeMap<Integer, Number> expectedResult = new TreeMap<>();
        expectedResult.put(2, 12.0); // 12x^2
        expectedResult.put(1, 8.0); // 8x
        expectedResult.put(0, -20.0); // -20

        assertEquals(expectedResult, result.getTerms());
    }

    @Test
    public void testExecute_MultiplyByIdentity_ReturnsSamePolynomial() {
        // Arrange
        Polynomial polynomial1 = new Polynomial();
        polynomial1.addTerm(2, 3); // 3x^2
        polynomial1.addTerm(1, 2); // 2x
        polynomial1.addTerm(0, -5); // -5

        Polynomial polynomial2 = new Polynomial();
        polynomial2.addTerm(0, 1); // 1 (identity)

        // Act
        Polynomial result = MultiplicationOP.execute(polynomial1, polynomial2);

        // Assert
        TreeMap<Integer, Number> expectedResult = new TreeMap<>();
        expectedResult.put(2, 3.0); // 3x^2
        expectedResult.put(1, 2.0); // 2x
        expectedResult.put(0, -5.0); // -5

        assertEquals(expectedResult, result.getTerms());
    }
}


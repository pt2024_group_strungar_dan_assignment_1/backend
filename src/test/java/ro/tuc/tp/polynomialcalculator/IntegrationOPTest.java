package ro.tuc.tp.polynomialcalculator;

import org.junit.jupiter.api.Test;
import ro.tuc.tp.polynomialcalculator.model.IntegrationOP;
import ro.tuc.tp.polynomialcalculator.model.Polynomial;

import java.util.TreeMap;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class IntegrationOPTest {

    @Test
    public void testExecute_IntegratePolynomial() {
        // Arrange
        Polynomial polynomial = new Polynomial();
        polynomial.addTerm(2, 4); // 4x^2
        polynomial.addTerm(1, -6); // -6x
        polynomial.addTerm(0, 5); // 5

        // Act
        Polynomial result = IntegrationOP.execute(polynomial);

        // Assert
        TreeMap<Integer, Number> expectedResult = new TreeMap<>();
        expectedResult.put(3, 4.0 / 3); // 4/3x^3
        expectedResult.put(2, -6.0 / 2); // -3x^2
        expectedResult.put(1, 5.0 / 1); // 5x

        assertEquals(expectedResult, result.getTerms());
    }

    @Test
    public void testExecute_IntegratePolynomialWithConstantTerm() {
        // Arrange
        Polynomial polynomial = new Polynomial();
        polynomial.addTerm(0, 5); // 5

        // Act
        Polynomial result = IntegrationOP.execute(polynomial);

        // Assert
        TreeMap<Integer, Number> expectedResult = new TreeMap<>();
        expectedResult.put(1, 5.0 / 1); // 5x

        assertEquals(expectedResult, result.getTerms());
    }

    @Test
    public void testExecute_IntegratePolynomialWithLinearTerm() {
        // Arrange
        Polynomial polynomial = new Polynomial();
        polynomial.addTerm(1, -3); // -3x
        polynomial.addTerm(0, 2); // 2

        // Act
        Polynomial result = IntegrationOP.execute(polynomial);

        // Assert
        TreeMap<Integer, Number> expectedResult = new TreeMap<>();
        expectedResult.put(2, -3.0 / 2); // -3/2x^2
        expectedResult.put(1, 2.0 / 1); // 2x

        assertEquals(expectedResult, result.getTerms());
    }

    @Test
    public void testExecute_IntegrateConstantPolynomial() {
        // Arrange
        Polynomial polynomial = new Polynomial();
        polynomial.addTerm(0, -7); // -7

        // Act
        Polynomial result = IntegrationOP.execute(polynomial);

        // Assert
        TreeMap<Integer, Number> expectedResult = new TreeMap<>();
        expectedResult.put(1, -7.0 / 1); // -7x

        assertEquals(expectedResult, result.getTerms());
    }
}


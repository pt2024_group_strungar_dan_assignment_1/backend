package ro.tuc.tp.polynomialcalculator;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import ro.tuc.tp.polynomialcalculator.controller.Controller;
@ExtendWith(MockitoExtension.class)
@WebMvcTest(Controller.class)
class PolynomialCalculatorApplicationTests {

    @Autowired
    private MockMvc mockMvc;

    @InjectMocks
    private Controller parserController;

    @Test
    public void testGetResult() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/calculator")
                .param("polynomial1", "3x^2 + 2x - 5")
                .param("operation", "addition")
                .param("polynomial2", "x^3 - 4x^2 + 6x"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string(
                        "[-5 + 8.0x + -1.0x^2 + x^3]"));
    }

}

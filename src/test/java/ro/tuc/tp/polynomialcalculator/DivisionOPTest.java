package ro.tuc.tp.polynomialcalculator;

import org.junit.jupiter.api.Test;
import ro.tuc.tp.polynomialcalculator.model.DivisionOP;
import ro.tuc.tp.polynomialcalculator.model.Polynomial;
import ro.tuc.tp.polynomialcalculator.util.DivisionByZeroError;
import ro.tuc.tp.polynomialcalculator.util.DivisionOutputType;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class DivisionOPTest {

    @Test
    public void testExecute_DividePolynomials_ReturnQuotient() throws DivisionByZeroError {
        // Arrange
        Polynomial dividend = new Polynomial();
        dividend.addTerm(2, 6); // 6x^2
        dividend.addTerm(1, 5); // 5x
        dividend.addTerm(0, -15); // -15

        Polynomial divisor = new Polynomial();
        divisor.addTerm(1, 3); // 3x
        divisor.addTerm(0, -5); // -5

        // Act
        Polynomial quotient = DivisionOP.execute(dividend, divisor, DivisionOutputType.QUOTIENT);

        // Assert
        Polynomial expectedQuotient = new Polynomial();
        expectedQuotient.addTerm(1, 2.0); // 2x
        expectedQuotient.addTerm(0, -5.0);

        assertEquals(expectedQuotient.getTerms(), quotient.getTerms());
    }

    @Test
    public void testExecute_DividePolynomials_ReturnRemainder() throws DivisionByZeroError {
        // Arrange
        Polynomial dividend = new Polynomial();
        dividend.addTerm(3, 3); // 3x^3
        dividend.addTerm(2, -7); // -7x^2
        dividend.addTerm(1, 5); // 5x
        dividend.addTerm(0, 4); // 4

        Polynomial divisor = new Polynomial();
        divisor.addTerm(1, 1); // x
        divisor.addTerm(0, 2); // 2

        // Act
        Polynomial remainder = DivisionOP.execute(dividend, divisor, DivisionOutputType.REMAINDER);

        // Assert
        Polynomial expectedRemainder = new Polynomial();
        expectedRemainder.addTerm(0, 38.0); // 0

        assertEquals(expectedRemainder.getTerms(), remainder.getTerms());
    }

    @Test
    public void testExecute_DivideByZero_ThrowsDivisionByZeroError() {
        // Arrange
        Polynomial dividend = new Polynomial();
        dividend.addTerm(1, 4); // 4x

        Polynomial divisor = new Polynomial(); // Polynomial with degree 0

        // Act & Assert
        assertThrows(DivisionByZeroError.class, () -> DivisionOP.execute(dividend, divisor, DivisionOutputType.QUOTIENT));
    }

    @Test
    public void testExecute_DivideByZero_RemainderThrowsDivisionByZeroError() {
        // Arrange
        Polynomial dividend = new Polynomial();
        dividend.addTerm(1, 4); // 4x

        Polynomial divisor = new Polynomial(); // Polynomial with degree 0

        // Act & Assert
        assertThrows(DivisionByZeroError.class, () -> DivisionOP.execute(dividend, divisor, DivisionOutputType.REMAINDER));
    }
}


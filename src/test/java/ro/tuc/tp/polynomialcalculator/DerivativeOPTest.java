package ro.tuc.tp.polynomialcalculator;

import org.junit.jupiter.api.Test;
import ro.tuc.tp.polynomialcalculator.model.DerivativeOP;
import ro.tuc.tp.polynomialcalculator.model.Polynomial;

import java.util.TreeMap;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DerivativeOPTest {

    @Test
    public void testExecute_DerivativeOfPolynomial() {
        // Arrange
        Polynomial polynomial = new Polynomial();
        polynomial.addTerm(3, 2); // 2x^3
        polynomial.addTerm(2, 4); // 4x^2
        polynomial.addTerm(1, -3); // -3x
        polynomial.addTerm(0, 5); // 5

        // Act
        Polynomial result = DerivativeOP.execute(polynomial);

        // Assert
        TreeMap<Integer, Number> expectedResult = new TreeMap<>();
        expectedResult.put(2, 6); // 6x^2
        expectedResult.put(1, 8); // 8x
        expectedResult.put(0, -3); // -3

        assertEquals(expectedResult, result.getTerms());
    }

    @Test
    public void testExecute_DerivativeOfConstantPolynomial() {
        // Arrange
        Polynomial polynomial = new Polynomial();
        polynomial.addTerm(0, 5); // 5

        // Act
        Polynomial result = DerivativeOP.execute(polynomial);

        // Assert
        TreeMap<Integer, Number> expectedResult = new TreeMap<>();

        assertEquals(expectedResult, result.getTerms());
    }

    @Test
    public void testExecute_DerivativeOfLinearPolynomial() {
        // Arrange
        Polynomial polynomial = new Polynomial();
        polynomial.addTerm(1, 3); // 3x
        polynomial.addTerm(0, -4); // -4

        // Act
        Polynomial result = DerivativeOP.execute(polynomial);

        // Assert
        TreeMap<Integer, Number> expectedResult = new TreeMap<>();
        expectedResult.put(0, 3); // 3

        assertEquals(expectedResult, result.getTerms());
    }

    @Test
    public void testExecute_DerivativeOfPolynomialWithOnlyConstantTerm() {
        // Arrange
        Polynomial polynomial = new Polynomial();
        polynomial.addTerm(0, -2); // -2

        // Act
        Polynomial result = DerivativeOP.execute(polynomial);

        // Assert
        TreeMap<Integer, Number> expectedResult = new TreeMap<>();

        assertEquals(expectedResult, result.getTerms());
    }
}


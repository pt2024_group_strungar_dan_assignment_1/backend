package ro.tuc.tp.polynomialcalculator;

import org.junit.jupiter.api.Test;
import ro.tuc.tp.polynomialcalculator.model.Polynomial;
import ro.tuc.tp.polynomialcalculator.model.SubtractionOP;

import java.util.TreeMap;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SubtractionOPTest {

    @Test
    public void testExecute_SubtractPolynomials() {
        // Arrange
        Polynomial polynomial1 = new Polynomial();
        polynomial1.addTerm(2, 3); // 3x^2
        polynomial1.addTerm(1, 2); // 2x
        polynomial1.addTerm(0, -5); // -5

        Polynomial polynomial2 = new Polynomial();
        polynomial2.addTerm(2, 1); // x^2
        polynomial2.addTerm(1, -3); // -3x
        polynomial2.addTerm(0, 4); // 4

        // Act
        Polynomial result = SubtractionOP.execute(polynomial1, polynomial2);

        // Assert
        TreeMap<Integer, Number> expectedResult = new TreeMap<>();
        expectedResult.put(2, -2.0); // 2x^2
        expectedResult.put(1, -5.0); // 5x
        expectedResult.put(0, 9.0); // -9

        assertEquals(expectedResult, result.getTerms());
    }

    @Test
    public void testExecute_SubtractZeroPolynomial_ReturnsOriginalPolynomial() {
        // Arrange
        Polynomial polynomial1 = new Polynomial();
        polynomial1.addTerm(2, 3); // 3x^2
        polynomial1.addTerm(1, 2); // 2x
        polynomial1.addTerm(0, -5); // -5

        Polynomial polynomial2 = new Polynomial(); // Zero polynomial

        // Act
        Polynomial result = SubtractionOP.execute(polynomial1, polynomial2);

        // Assert
        TreeMap<Integer, Number> expectedResult = new TreeMap<>();
        expectedResult.put(2, 3); // 3x^2
        expectedResult.put(1, 2); // 2x
        expectedResult.put(0, -5); // -5

        assertEquals(expectedResult, result.getTerms());
    }

    @Test
    public void testExecute_SubtractFromZeroPolynomial_ReturnsNegativeOfSecondPolynomial() {
        // Arrange
        Polynomial polynomial1 = new Polynomial(); // Zero polynomial

        Polynomial polynomial2 = new Polynomial();
        polynomial2.addTerm(3, 4); // 4x^3
        polynomial2.addTerm(1, -2); // -2x
        polynomial2.addTerm(0, 5); // 5

        // Act
        Polynomial result = SubtractionOP.execute(polynomial1, polynomial2);

        // Assert
        TreeMap<Integer, Number> expectedResult = new TreeMap<>();
        expectedResult.put(3, -4.0); // -4x^3
        expectedResult.put(1, 2.0); // 2x
        expectedResult.put(0, -5.0); // -5

        assertEquals(expectedResult, result.getTerms());
    }

    @Test
    public void testExecute_SubtractPolynomialFromItself_ReturnsZeroPolynomial() {
        // Arrange
        Polynomial polynomial1 = new Polynomial();
        polynomial1.addTerm(2, 3); // 3x^2
        polynomial1.addTerm(1, 2); // 2x
        polynomial1.addTerm(0, -5); // -5

        // Act
        Polynomial result = SubtractionOP.execute(polynomial1, polynomial1);

        // Assert
        TreeMap<Integer, Number> expectedResult = new TreeMap<>();
        expectedResult.put(0, 0.0); // Result should be zero polynomial
        expectedResult.put(1, 0.0); // Result should be zero polynomial
        expectedResult.put(2, 0.0); // Result should be zero polynomial

        assertEquals(expectedResult, result.getTerms());
    }
}

